(function(){
	var Cloud = require('modules/ti.cloud');
	var constants = require('common/Constants');
	var style = require('style/alarmStyle');
	
	if(Ti.App.Properties.hasProperty(constants["IS_NEED_NOTIFY"])) {
		Ti.App.iOS.cancelAllLocalNotifications();
		
		var alertTime = Ti.App.Properties.getString(constants["ALERT_TIME"]);		
		var date = new Date(alertTime);
		var notification_params = style["notification"];
		notification_params["date"] = date;
		
		Ti.App.Properties.removeProperty(constants["IS_NEED_NOTIFY"]);
		Ti.App.iOS.scheduleLocalNotification(notification_params);
	}
		
	Ti.App.currentService.stop();
})();
