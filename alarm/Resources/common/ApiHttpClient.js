var ApiHttpClient = function() {
    var client = Titanium.Network.createHTTPClient();
    this.postShowOfflineDialogHook = function(){};
    this.encoding = 'UTF-8';
    //this.apiServerPath = 'http://guarded-everglades-9000.herokuapp.com/';
   this.apiServerPath = 'https://intense-crag-3900.herokuapp.com/';
   // this.apiServerPath = 'http://localhost:3000/';
    client.onerror = function(error) {
        Titanium.UI.createAlertDialog({
            title : "通信に失敗しました。",
            buttonNames : ['OK']
        }).show();
        Ti.API.debug(error);
        postOnErrorHook();
    }

    this.setOnload = function(success) {
        client.onload = success;
    }

    this.setOnerror = function(error) {
        client.onerror = error;
    }

    this.postOnErrorHook = function(postProcess) {
        postProcess();
    }
    /**
     * GETで通信を行う
     * @param apiPath 通信先APIパス(アプリケーションルートからのパス)
     * @param paramArray JSON形式でパラメータ指定
     */
    this.get = function(apiPath, paramArray) {
        if (!this.isOnline()) {
            return;
        }
        var params = '';
        if (paramArray) {
            params = '?';
        }
        for (var key in paramArray) {
            params = params + key + '=' + paramArray[key];
            params = params + '&';
        }
        // androidだと、open前にsetRequestHeaderするとエラーが出るので注意
        // ********************************* エラーログ **********************
        // Message: Wrapped java.lang.IllegalStateException: setRequestHeader can only be called before invoking send.
        // (file:///android_asset/Resources/util/ApiHttpClient.js#16)
        // ********************************* エラーログ **********************
        client.open('GET', this.apiServerPath + apiPath + params);      
        client.setRequestHeader('Content-Type', 'application/json', 'charset=' + this.encoding + ';');
        client.send();
    }

    this.post = function(apiPath, paramArray) {
        if (!this.isOnline()) {
            return;
        }
        client.open('POST', this.apiServerPath + apiPath);
        client.setRequestHeader('Content-Type', 'application/json', 'charset=' + this.encoding + ';');
        if (paramArray) {
            client.send(JSON.stringify(paramArray));
        } else {
            client.send();
        }
        Ti.API.info('API call: ' + apiPath + ' params: ');
        Ti.API.info(paramArray);
    }

    this.viewOfflineDialog = function() {
        Titanium.UI.createAlertDialog({
            title : "エラー",
            message : "インターネットに接続できません。",
            buttonNames : ['OK']
        }).show();
        this.postShowOfflineDialogHook();
    }

    this.isOnline = function() {
        if (!Titanium.Network.online) {
            this.viewOfflineDialog();
            return false;
        }
        return true;
    }

    this.setPostShowOfflineDialogHook = function(e) {
    	this.postShowOfflineDialogHook = e;
    }

    this.close = function() {
        client.onload = null;
        client.onerror = null;
        client.onreadystatechange = null;
        client.ondatastream = null;
        client = null;
    }
}
module.exports = ApiHttpClient;