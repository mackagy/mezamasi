//定数のキー値
exports.ALERT_TIME = "alertTime";
exports.DEVICE_TOKEN = "deviceToken";
exports.FB_NAME_KEY = "fbnamekey";
exports.MEZAMASI_ID_KEY ="mezamasiIdKey";
exports.IS_NEED_NOTIFY = "isNeetNotify";

//APIパス
exports.API_PATH_MEMBER_DECISION = "members/decision.json";
exports.API_PATH_MEMBER_REGIST = "members/regist.json";
exports.API_PATH_MEMBER_TOKEN_REGIST = "members/tokenregist.json";
exports.API_PATH_NOTICE_RESERVATION = "notices/reservation.json";
exports.API_PATH_NOTICE_DEFEASIBLE = "notices/defeasible.json";
// 友達一覧取得
exports.API_PATH_FRIENDS_LIST = "friends/list";
// プッシュ通知設定
exports.API_PATH_FRIENDS_SETPUSH = "friends/setpush";
// 電話番号更新
exports.API_PATH_UPDATE_TEL = "members/noupdate";
// 電話番号保持確認
exports.API_PATH_HAS_TEL ="members/hastel";
// 架電認証要求
exports.API_PATH_TEL_AUTH_REQUEST = "notices/telauthrequest";
// シリアルコード認証
exports.API_PATH_TEL_AUTH = "notices/telauth";

//もろもろ定数
exports.FACEBOOK_APPID = "421058484617224";
exports.SET_PUSH_TRUE = "1";