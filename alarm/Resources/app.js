(function(){
	var Cloud = require('modules/ti.cloud');
	var constants = require('common/Constants');
	var ApiHttpClient = require('common/ApiHttpClient');

	// デバイストークン取得・設定
	Titanium.Network.registerForPushNotifications({
		// アラートを許可
		types: [
			Titanium.Network.NOTIFICATION_TYPE_ALERT
	    ],
	    // デバイストークン取得成功時、デバイストークンを保存する
	    success:function(e) {
			Ti.App.Properties.setString(constants["DEVICE_TOKEN"], e.deviceToken);
		
		},
		// PushNotificationを受け取った際、ポップアップ表示を行う
		callback:function(e)  {
			var dialog = Ti.UI.createAlertDialog({
				title: e.data.title,
				message: e.data.alert,
				buttonNames: ['オコす','ホウチ']});
			dialog.show();
			dialog.addEventListener('click', function(e){
				if (e.index == 0) {
					var message = e.source.message;
					var messageArr = message.split(" ");
					var sendMezamasiId = messageArr[messageArr.length - 1];
					sendMezamasiId = sendMezamasiId.replace(/\[|\]/g, "");
					var client = new ApiHttpClient();
					client.get('notices/tel', {
		            	'mezamasi_id' : Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']),
		            	'send_mezamasi_id' : sendMezamasiId
		            });			
					client.setOnload(function(){
						var json = JSON.parse(this.responseText);       		
						if (json.result == 'NG') {
							new ErrorPopup("オコすのに失敗しました。。");
							return;
						} else if (json.result == 'OK2') {
							 Ti.UI.createAlertDialog({
								message: "相手が電話番号未登録です。直接オコシてあげてください。",
								buttonNames: ['OK']}).show();
							return;
						}
						Ti.UI.createAlertDialog({
							message: "電話をかけました。オキるといいですね。",
							buttonNames: ['OK']}).show();
					});
					// 失敗時
		            client.setOnerror(function(){
						new ErrorPopup("オコすのに失敗しました。。");
						return;
		            });	
				}
			});
		}
	});

	 //-- ログイン --//
	 var LoginWindow = require('ui/common/loginWindow');
	 //-- メイン画面 --//
	 var MainWindow = require('ui/common/mainWindow');

	//-- バックグラウンドで実行するサービスの登録 --//
	Ti.App.iOS.registerBackgroundService({url: 'Service.js'});
	// //-- 停止復帰時のイベント設定 --//
	// Ti.App.addEventListener("resume", function() {
	    // // 通知キャンセル
	    // Ti.App.iOS.cancelAllLocalNotifications();
	// });
	
	var mainWindow = new MainWindow();	
	var loginWindow = new LoginWindow();
	
	Ti.App.addEventListener('App_Logout',function(e){		
		mainWindow.close();
		loginWindow.open();
	});
	
	Ti.App.addEventListener('App_Login', function(e){
		loginWindow.close();
		mainWindow.open();
	});
	
	// ログイン中の場合メイン画面を開く
	 if (Ti.Facebook.loggedIn) {
	 	mainWindow.open();
	// 未ログインの場合ログイン画面を開く
	} else {
		loginWindow.open();
	}
	
})();
