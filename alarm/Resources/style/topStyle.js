var centerHeight = Titanium.Platform.displayCaps.platformHeight / 2;

exports.window = {
	title:'TOP',
	barColor:'#245E8C',
    backgroundColor:'#245E8C'
}
exports.initView = {}
exports.alarmView = {}
exports.alarmLabel = {
	top: centerHeight - 90,
	font: {fontSize: 30,fontWeight: 'bold'}
}
exports.nojikanImage = {
	image:'images/nojikan.png',
	height: 300,
	width: 300,
	left: 30
}
exports.jikanImage = {
	image:'images/jikan.png',
	height: 300,
	width: 300,
	left: 30
}
exports.tab = {
	title:'TOP',
	icon:'images/top.png',
}
