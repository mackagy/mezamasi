exports.window = {
	title : '設定',
	barColor : '#245E8C',
	backgroundColor : '#245E8C'
}
exports.phoneView = {
	top : 0,
	height : 100
}
exports.telField = {
	hintText: '例：09012345678',
	top : 10,
	height : 30,
	width : 200,
	keyboardType : Ti.UI.KEYBOARD_NUMBERS_PUNCTUATION,
	returnKeyType : Ti.UI.RETURNKEY_DONE,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
}
exports.keyField = {
	hintText: '例：12345',
	top : 10,
	height : 30,
	width : 200,
	keyboardType : Ti.UI.KEYBOARD_NUMBERS_PUNCTUATION,
	returnKeyType : Ti.UI.RETURNKEY_DONE,
	borderStyle : Ti.UI.INPUT_BORDERSTYLE_ROUNDED
}
exports.alreadyRegistImage = {
	top : 45,
	backgroundImage : 'images/alreadyRegistLabel.png',
	height : 50,
	width : 200
}
exports.setumeiImage = {
	top : 100,
	backgroundImage : 'images/setumeiLabel.png',
	height : 130,
	width : 260
}
exports.authRequestButton = {
	top : 45,
	backgroundImage : 'images/authRequestButton.png',
	height : 55,
	width : 200
}
exports.kiyakuButton = {
	top : 235,
	height : 55,
	width : 200,	
	backgroundImage : 'images/kiyakuButton.png'
}
exports.batuButton = {
	top : 10,
	right: 10,
	height : 50,
	width : 50,	
	backgroundImage : 'images/batuButton.png'
}
exports.authRequestDialog = {
	title : '電話番号認証要求',
	message : 'ご入力頂いた電話番号に認証番号を知らせる音声案内が発信されます。ご案内させていただきました認証番号を利用することで電話番号が登録可能となります。音声案内を発信して宜しいですか？',
	buttonNames : ['OK', 'キャンセル'],
	cancel : 1
}
exports.registButton = {
	top : 45,
	backgroundImage : 'images/telRegistButton.png',
	height : 55,
	width : 200
}
exports.registDialog = {
	title : '電話番号認証',
	message : '音声案内にてお伝えした認証番号をご入力頂くことで、電話番号が登録できます。電話番号を登録して宜しいですか？',
	buttonNames : ['OK', 'キャンセル'],
	cancel : 1
}
exports.authSuccessDialog = {
	title : '電話番号認証成功',
	message : '電話番号の認証に成功しました。電話番号の登録を行います。',
	buttonNames : ['OK']
}
exports.registSuccessDialog = {
	title : '電話番号登録成功',
	message : '電話番号の登録に成功しました！オコシテをお楽しみください！',
	buttonNames : ['OK']
}
exports.facebookLogoutButton = {
	bottom : 20,
	backgroundImage : 'images/facebookLogoutButton.png',
	height : 55,
	width : 200
}
exports.tab = {
	title : '設定',
	icon : 'images/setting.png'
}
