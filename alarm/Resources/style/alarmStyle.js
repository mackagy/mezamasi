exports.window = {
	title:'オコシテ',
	barColor:'#245E8C',
    backgroundColor:'#245E8C'
}
exports.alarmPicker = {}
exports.alarmPickerView = {
	top: 0,
	height: 300,
	width:300
}
exports.alarmPickerBaseView = {}
exports.alarmPickerBottomView = {
	bottom: 20,
	width: 300,
	height: 100
}
exports.wakeupButtonView = {}
exports.submitButton = {
	backgroundImage:'images/okositeButton.png',
	height: 55,
	width: 200
}
exports.wakeupButton = {
	backgroundImage:'images/okiruButton.png',
	height: 55,
	width: 200
}
exports.notification = {
	alertBody: "オキる時間です。。",
	alertAction: 'OK',
	sound: 'default'
}
exports.wakeupSuccessDialog = {
	title:'オキた',
	message:'オキれましたね！',
	buttonNames:['OK']
}
exports.wakeupFailureDialog = {
	title:'ネボウした',
	message:'オコシテもらえたか？',
	buttonNames:['OK']	
}
exports.tab = {
	title:'オコシテ',
	icon:'images/alarm.png'
}
