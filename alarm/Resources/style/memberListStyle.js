exports.window = {
	title:'メンバー',
	barColor:'#245E8C',
    backgroundColor:'#245E8C'
}
exports.tab = {
	title:'メンバー',
	icon:'images/memberList.png'
}
exports.userImg = {
	left:5,
	width:50,
	height:50
}
exports.nameLabel = {
	color:'#F1EFE8',
	left:70
}
exports.alarmSwitch = {
	right:10,
	touchEnabled:true
}
exports.row = {
	height:60,
	touchEnabled:false,
	selectionStyle:Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
}
exports.tableView = {
	backgroundColor:'#245E8C',
	separatorColor:'#245E8C',
}