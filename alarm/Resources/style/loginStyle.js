exports.window = {
	title:'ログイン',
	barColor:'#245E8C',
    backgroundColor:'#245E8C'
}
exports.titleImage = {
	image:'images/title.png',
	height: 300,
	width: 300,
	top: 20
}
exports.facebookLoginButton = {
	backgroundImage:'images/facebookLoginButton.png',
	height: 55,
	width: 200,
	top: 380
}
