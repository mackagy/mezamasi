	function SettingWindow() {
	// 必要なモジュールを読み込み
	var style = require('style/settingStyle');
	var constants = require('common/Constants');
	var ApiHttpClient = require('common/ApiHttpClient');
	var IndicatorView = require('ui/common/indicatorView');
	var ErrorPopup = require('ui/common/errorPopup');
	var LoginWindow = require('ui/common/loginWindow');

	// ベースウインドウ作成
	var window = Ti.UI.createWindow(style['window']);

	// ログアウトボタン
	var logoutButton = Ti.UI.createButton(style["facebookLogoutButton"]);
	logoutButton.addEventListener('click', function() {
		Ti.Facebook.logout();
		Ti.App.Properties.removeProperty(constants["FB_NAME_KEY"]);
		Ti.App.Properties.removeProperty(constants["MEZAMASI_ID_KEY"]);
		Ti.App.fireEvent("App_Logout");
	});

	// インジケータ
	var indicatorView;

	// 電話認証ビュー
	var phoneView;

	// 電場番号入力フィールド
	var telField;

	// シリアルコード入力フィールド
	var keyField;

	// 登録済みイメージ
	var alreadyImage;

	// 説明文
	var setumeiImage = Ti.UI.createImageView(style['setumeiImage']);
	window.add(setumeiImage);

	// 利用規約
	var kiyakuButton = Ti.UI.createButton(style['kiyakuButton']);

	var webView = Titanium.UI.createWebView( {
		url: "http://mighty-island-1488.herokuapp.com/informations/terms.html"
	});
	var batu = Ti.UI.createButton(style['batuButton']);
	webView.add(batu);
	batu.addEventListener('click',function(e){
		window.remove(webView);
	})

	kiyakuButton.addEventListener('click', function(e) {
		window.add(webView);
	})

	// 目覚ましID
	var id = null;

	// 電話番号
	var telNo = null;

	window.addEventListener('focus', function(e) {
		id = Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']);
		Ti.App.fireEvent("Has_Tel");
	});

	// 電話番号保持確認イベント
	Ti.App.addEventListener('Has_Tel', function() {
		indicatorView = new IndicatorView();
		window.add(indicatorView);
		indicatorView.showInd();
		var id = Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']);
		var http = new ApiHttpClient();
		http.setPostShowOfflineDialogHook(function() {
			indicatorView.hideInd();
		});
		http.get(constants['API_PATH_HAS_TEL'], {
			'mezamasi_id' : id
		});
		// 失敗時
		http.setOnerror(function() {
			indicatorView.hideInd();
			new ErrorPopup("通信に失敗しました。もう一度お試しください。");
			return;
		});
		http.setOnload(function() {
			var result = JSON.parse(this.responseText).result;
			var hasTel = JSON.parse(this.responseText).hastel;
			if (result == "NG") {
				indicatorView.hideInd();
				new ErrorPopup("通信に失敗しました。もう一度お試しください。");
				return;
			}
			phoneView = Ti.UI.createView(style["phoneView"]);
			if (hasTel == "0") {
				// 認証要求ボタン
				var registButton = Ti.UI.createButton(style["authRequestButton"]);
				registButton.addEventListener('click', function() {
					var registDialog = Ti.UI.createAlertDialog(style["authRequestDialog"]);
					registDialog.show();
					registDialog.addEventListener('click', function(e) {
						if (e.index == 0) {
							Ti.App.fireEvent("Auth_Request");
						}
					});
				});
				phoneView.add(registButton);
				telField = Ti.UI.createTextField(style["telField"]);
				phoneView.add(telField);
				window.add(phoneView);
			} else if (hasTel == "1") {
				alreadyImage = Ti.UI.createImageView(style['alreadyRegistImage']);
				window.add(alreadyImage);
			} else {
				indicatorView.hideInd();
				new ErrorPopup("通信に失敗しました。もう一度お試しください。");
				return;
			}
			window.add(kiyakuButton);
			window.add(logoutButton);
			indicatorView.hideInd();
		});
	});

	// 架電認証要求イベント
	Ti.App.addEventListener('Auth_Request', function() {
		window.remove(indicatorView);
		indicatorView = new IndicatorView();
		window.add(indicatorView);
		indicatorView.showInd();
		telNo = telField.value;
		if (telNo == null || telNo == '') {
			indicatorView.hideInd();
			new ErrorPopup("電話番号を入力して下さい。");
			return;
		}
		var id = Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']);
		var http = new ApiHttpClient();
		http.setPostShowOfflineDialogHook(function() {
			indicatorView.hideInd();
		});
		http.get(constants['API_PATH_TEL_AUTH_REQUEST'], {
			'mezamasi_id' : id,
			'no' : telNo
		});
		// 失敗時
		http.setOnerror(function() {
			indicatorView.hideInd();
			new ErrorPopup("通信に失敗しました。もう一度お試しください。");
			return;
		});
		http.setOnload(function() {
			var result = JSON.parse(this.responseText).result;
			if (result == "NG") {
				indicatorView.hideInd();
				new ErrorPopup("架電に失敗しました。もう一度お試しください。");
				return;
			}
			phoneView.hide();
			phoneView = Ti.UI.createView(style["phoneView"]);
			keyField = Ti.UI.createTextField(style["keyField"]);
			phoneView.add(keyField);
			// 登録ボタン
			var registButton = Ti.UI.createButton(style["registButton"]);
			registButton.addEventListener('click', function() {
				var registDialog = Ti.UI.createAlertDialog(style["registDialog"]);
				registDialog.show();
				registDialog.addEventListener('click', function(e) {
					if (e.index == 0) {
						Ti.App.fireEvent("Tel_Auth");
					}
				});
			});
			phoneView.add(registButton);
			window.add(phoneView);
			indicatorView.hideInd();
		});
	});

	// シリアルコード認証イベント
	Ti.App.addEventListener('Tel_Auth', function(e) {
		window.remove(indicatorView);
		indicatorView = new IndicatorView();
		window.add(indicatorView);
		indicatorView.showInd();
		authKey = keyField.value;
		if (authKey == null) {
			indicatorView.hideInd();
			new ErrorPopup("シリアルコードを入力して下さい。");
			return;
		}
		var id = Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']);
		var http = new ApiHttpClient();
		http.setPostShowOfflineDialogHook(function() {
			indicatorView.hideInd();
		});
		http.get(constants['API_PATH_TEL_AUTH'], {
			'mezamasi_id' : id,
			'authkey' : authKey
		});
		// 失敗時
		http.setOnerror(function() {
			indicatorView.hideInd();
			new ErrorPopup("通信に失敗しました。もう一度お試しください。");
			return;
		});
		http.setOnload(function() {
			var result = JSON.parse(this.responseText).result;
			if (result == "NG") {
				indicatorView.hideInd();
				new ErrorPopup("認証に失敗しました。もう一度お試しください。");
				return;
			}
			var authSuccessDialog = Ti.UI.createAlertDialog(style["authSuccessDialog"]);
			indicatorView.hideInd();
			authSuccessDialog.show();
			authSuccessDialog.addEventListener('click', function() {
				Ti.App.fireEvent("Tel_Update");
			});
		});
	});

	// 電話番号登録イベント
	Ti.App.addEventListener('Tel_Update', function() {
		window.remove(indicatorView);
		indicatorView = new IndicatorView();
		window.add(indicatorView);
		indicatorView.showInd();
		telNo = telField.value;
		var http = new ApiHttpClient();
		http.setPostShowOfflineDialogHook(function() {
			indicatorView.hideInd();
		});
		http.get(constants['API_PATH_UPDATE_TEL'], {
			'mezamasi_id' : id,
			'no' : telNo
		});
		// 失敗時
		http.setOnerror(function() {
			indicatorView.hideInd();
			new ErrorPopup("通信に失敗しました。もう一度お試しください。");
			return;
		});
		http.setOnload(function() {
			var result = JSON.parse(this.responseText).result;
			if (result == "NG") {
				indicatorView.hideInd();
				new ErrorPopup("トウロクに失敗しました。もう一度お試しください。");
				return;
			}
			phoneView.hide();
			alreadyImage = Ti.UI.createImageView(style['alreadyRegistImage']);
			window.add(alreadyImage);
			var registSuccessDialog = Ti.UI.createAlertDialog(style["registSuccessDialog"]);
			indicatorView.hideInd();
			registSuccessDialog.show();
		});
	});

	window.addEventListener('blur', function() {
		window.remove(indicatorView);
	});

	return window;
}

module.exports = SettingWindow;
