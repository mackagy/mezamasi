function LoginWindow() {

	// 必要なモジュールを読み込み
	var constants = require('common/Constants');
	var style = require('style/loginStyle');
	var MainWindow = require('ui/common/mainWindow');
	var LoginWindow = require('ui/common/loginWindow');
	var IndicatorView = require('ui/common/indicatorView');
	var ErrorPopup = require('ui/common/errorPopup');
	var ApiHttpClient = require('common/ApiHttpClient');
	var Cloud = require('modules/ti.cloud');
	
	// ベースウインドウ作成
	var window = Ti.UI.createWindow(style['window']);
	
	// タイトル画像作成
	var titleImage = Titanium.UI.createImageView(style['titleImage']); 
	window.add(titleImage);
	
	// facebookログインボタン作成
	var loginButton = Ti.UI.createButton(style["facebookLoginButton"]);
	loginButton.addEventListener('click',function(){
	    if ( Ti.Facebook.loggedIn ) {
	        Ti.Facebook.logout();
	    } else {
	        Ti.Facebook.authorize();
	    }
	});
	window.add(loginButton);
	
	// インジケータ表示用ビュー作成
	var indicarorView = new IndicatorView();
	window.add(indicarorView);
	
	// facebook通信系の設定
	Ti.Facebook.appid = constants['FACEBOOK_APPID'];
	Ti.Facebook.permissions = ['publish_stream'];
	
	// ACSのユーザ名(APIで発行)
	var acsUsername = null;
	// ACSのパスワード(APIで発行)
	var acsPassword = null;
	// ACSのユーザID(ACSで発行)
	var acsUserid = null;
	var facebookid = null;
	var mezamasiid = null;

	// facebookログイン時のイベント設定
	Ti.Facebook.addEventListener('login', function(e) {	
		indicarorView.showInd();
		
		// 認証成功
	    if (e.success) {   	
	    	// FACEBOOKユーザ名を設定
	    	Ti.App.Properties.setString(constants["FB_NAME_KEY"],e.data.last_name + e.data.first_name);
	    	facebookid = e.data.id;
	    	// 会員判定APIの呼び出し
	    	var client = new ApiHttpClient();
			client.setPostShowOfflineDialogHook(function() {
				indicarorView.hideInd();
				Ti.Facebook.logout();	
			});
			client.get(constants["API_PATH_MEMBER_DECISION"], {
            	'uid' : facebookid
        	});
        	//通信成功時の処理
        	client.setOnload(function() {
        		var json = JSON.parse(this.responseText);
        		if (json.result == 'NG') {
        			new ErrorPopup("会員判定に失敗しました。もう一度お試しください。");
					indicarorView.hideInd();
					Ti.Facebook.logout();
        		}
        		
        		acsUsername = json.mezamasi_id;
        		acsPassword = json.password;
        		acsUserid = json.acs_id;
        		mezamasiid = json.mezamasi_id;
   		
				// 会員未登録である場合
        		if (json.acs_id == null) {
 		    		// ACS上のユーザ登録を実施
					// ユーザ名-> 一意(ID)、パスワード-> 乱数
					Cloud.Users.create(
						{username: acsUsername, password: acsPassword, password_confirmation: acsPassword}
						,function (e) {	
						    if (e.success) {
								acsUserid = e.users[0].id;
								//ユーザ登録に成功したらデバイス登録を行う
								subscribeDeviceToACS();
						    } else {
								new ErrorPopup("会員登録に失敗しました。もう一度お試しください。");
								indicarorView.hideInd();
								Ti.Facebook.logout();
						    }
						}	    
					);       		
        		// 既に会員である場合
        		} else {
					//ログインする!
					loginToACS(acsUsername, acsPassword);
	    		}
			});
			client.setOnerror(function(){
				new ErrorPopup("会員判定に失敗しました。もう一度お試しください。 ");
				indicarorView.hideInd();
				Ti.Facebook.logout();
			});
		} else {
			indicarorView.hideInd();
		}
	});
	    
	// ACSへデバイス情報を登録
	var subscribeDeviceToACS = function() {
		Cloud.PushNotifications.subscribe({
			channel: 'friend_request',
			device_token: Ti.App.Properties.getString(constants["DEVICE_TOKEN"])
		}, function (e) {
			if (e.success) {
				// 会員登録APIの呼び出し
	    		var client = new ApiHttpClient();
				client.setPostShowOfflineDialogHook(function() {
					indicarorView.hideInd();
					Ti.Facebook.logout();	
				});
				client.get(constants['API_PATH_MEMBER_REGIST'], {
            		'uid' : facebookid,
            		'access_token' : Ti.Facebook.accessToken,
            		'mezamasi_id' : mezamasiid,
            		'pass' : acsPassword,
            		'acs_id' : acsUserid/*,
            		'device_token' : Ti.App.Properties.getString(constants["DEVICE_TOKEN"])*/
            	});
         		// 会員登録が成功した場合、ACSへログイン
        		client.setOnload(function() {
         			var json = JSON.parse(this.responseText);       		
	        		if (json.result == 'NG') {
	        			new ErrorPopup("会員登録に失敗しました。もう一度お試しください。");
						indicarorView.hideInd();
						Ti.Facebook.logout();
	        			return;
					}	
					loginToACS(acsUsername, acsPassword);
				});
	            // 失敗時
	            client.setOnerror(function(){
	            	new ErrorPopup("会員登録に失敗しました。もう一度お試しください。");
	            	indicarorView.hideInd();
					Ti.Facebook.logout();
	            });
			} else {
				new ErrorPopup("デバイス登録に失敗しました。もう一度お試しください。");
				indicarorView.hideInd();
				Ti.Facebook.logout();
			}
		});		
	}
	
	// ACSへのログイン
	var loginToACS = function(acsUsername, acsPassword) {
		//パスワード/ログインIDをAPIから取得してACSへ自動ログイン
		Cloud.Users.login({login: acsUsername,password: acsPassword}
		,function (e) {
			if (e.success) {
				// Facebookのトークン延長
				var client = new ApiHttpClient();
				client.setPostShowOfflineDialogHook(function() {
					indicarorView.hideInd();
					Ti.Facebook.logout();	
				});
				client.get(constants['API_PATH_MEMBER_TOKEN_REGIST'], {
					'mezamasi_id' : mezamasiid  
					,'access_token' : Ti.Facebook.accessToken,
				});
	            // 成功時
	            client.setOnload(function(){
	            	var json = JSON.parse(this.responseText);       		
		        	if (json.result == 'NG') {
		        		new ErrorPopup("facebookトークン延長に失敗しました。もう一度お試しください。");
		        		indicarorView.hideInd();
		        		Ti.Facebook.logout();
		        		return;
					}	            	
					Ti.App.Properties.setString(constants['MEZAMASI_ID_KEY'], mezamasiid);
					indicarorView.hideInd();
					// ログイン(・∀・)キタコレ！
					Ti.App.fireEvent("App_Login");
	            });
	            // 失敗時
	            client.setOnerror(function(){
	            	new ErrorPopup("facebookトークン延長に失敗しました。もう一度お試しください。");
	            	indicarorView.hideInd();
					Ti.Facebook.logout();
	            });
			} else {
				new ErrorPopup("ログインに失敗しました。もう一度お試しください。");
				indicarorView.hideInd();
				Ti.Facebook.logout();
			}
		});			
	}

	return window;
}
module.exports = LoginWindow;
