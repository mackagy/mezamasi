function IndicatorView() {
	
	var hiddenView = Ti.UI.createView({
		backgroundColor : "#000",
		opacity : "0.5"
	});
	var actInd = Titanium.UI.createActivityIndicator({
		style : Titanium.UI.iPhone.ActivityIndicatorStyle.BIG,
	});
	
	//----- メイン処理 -----//
	hiddenView.add(actInd);
	hiddenView.hideInd = function() {
		actInd.hide();
		hiddenView.hide();
	}
	hiddenView.showInd = function() {
		actInd.show()
		hiddenView.show();
	}
	hiddenView.hideInd();
	return hiddenView;
}
module.exports = IndicatorView;