function MainWindow() {
	//----- タブより遷移可能な画面用スタイル -----/
	//-- TOP画面 --//
	var topStyle = require('style/topStyle');
	var TopWindow = require('ui/common/topWindow');
	//-- メンバー一覧 --//
	var memberListStyle = require('style/memberListStyle');
	var MemberListWindow = require('ui/common/memberListWindow');
	//-- アクティビティ --//
//	var activityStyle = require('style/ActivityStyle');
//	var ActivityWindow = require('ui/common/ActivityWindow');
	//-- アラーム --//
	var alarmStyle = require('style/alarmStyle');
	var AlarmWindow = require('ui/common/alarmWindow');
	//-- 設定 --//
	var settingStyle = require('style/settingStyle');
	var SettingWindow = require('ui/common/settingWindow');
	
	//----- タブ作成 -----//
	// タブグループ
	var tabGroup = Titanium.UI.createTabGroup();
	// トップ用タブ
	var topTab = Titanium.UI.createTab(topStyle['tab']);
	topTab.window = new TopWindow;
	// メンバ一覧用タブ
	var memberListTab = Titanium.UI.createTab(memberListStyle['tab']);
	memberListTab.window = new MemberListWindow;
	// アクティビティ用タブ
//	var activityTab = Titanium.UI.createTab(activityStyle['tab']);
//	activityTab.window = new ActivityWindow;
	// アラーム設定
	var alarmTab = Titanium.UI.createTab(alarmStyle['tab']);
	alarmTab.window = new AlarmWindow;
	// 設定
	var settingTab = Titanium.UI.createTab(settingStyle['tab']);
	settingTab.window = new SettingWindow;
	
	tabGroup.addTab(topTab);
	tabGroup.addTab(memberListTab);
//	tabGroup.addTab(activityTab);
	tabGroup.addTab(alarmTab);
	tabGroup.addTab(settingTab);
	
	return tabGroup;
}
module.exports = MainWindow;
