function MemberListWindow() {
	// モジュール読み込み
	var style = require('style/memberListStyle');
	var constants = require('common/Constants');
	var ApiHttpClient = require('common/ApiHttpClient');
	var IndicatorView = require('ui/common/indicatorView');
	var ErrorPopup = require('ui/common/errorPopup');

	var win = Ti.UI.createWindow(style['window']);

	// インジケータ
	var indicatorView = new IndicatorView();

	// 友達リスト
	var tableView = Titanium.UI.createTableView(style['tableView']);

	// 友達のID
	var friendId = null;

	// スイッチのon/off
	var switchFlag = null;

	// 友達一覧作成
	win.addEventListener('focus', function(e) {
		win.add(indicatorView);
		indicatorView.showInd();
		// ユーザ自身のID
		var id = Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']);
		var http = new ApiHttpClient();
		http.setPostShowOfflineDialogHook(function() {
			indicatorView.hideInd();
		});
		http.get(constants['API_PATH_FRIENDS_LIST'], {
			'mezamasi_id' : id
		});
		// 失敗時
		http.setOnerror(function() {
			indicatorView.hideInd();
			new ErrorPopup("通信に失敗しました。もう一度お試しください。");
			return;
		});
		http.setOnload(function() {
			var result = JSON.parse(this.responseText).result;
			var friendResult = JSON.parse(this.responseText).list;
			if (result == "RETRY" || result == "NG") {
				indicatorView.hideInd();
				new ErrorPopup("メンバーの取得に失敗しました。もう一度お試しください。");
				return;
			}
			var friendList = [];
			for (var i = 0; i < friendResult.length; i++) {
				var userImg = Titanium.UI.createImageView(style['userImg']);
				userImg.image = 'https://graph.facebook.com/' + friendResult[i].uid + '/picture?type=small';

				var nameLabel = Titanium.UI.createLabel(style['nameLabel']);
				nameLabel.text = friendResult[i].name;

				var alarmSwitch = Titanium.UI.createSwitch(style['alarmSwitch']);
				if (friendResult[i].is_push == constants['SET_PUSH_TRUE']) {
					alarmSwitch.value = true;
				} else {
					alarmSwitch.value = false;
				}
				alarmSwitch.friendId = friendResult[i].mezamasi_id;
				// ON/OFF切り替え
				alarmSwitch.addEventListener('change', function(e) {
					friendId = e.source.friendId;
					switchFlag = e.value;
					Ti.App.fireEvent("Set_Push");
				});
				var row = Titanium.UI.createTableViewRow(style['row']);
				row.add(userImg);
				row.add(nameLabel);
				row.add(alarmSwitch);
				friendList.push(row);
			}
			tableView.data = friendList;
			win.add(tableView);
			indicatorView.hideInd();
		});
	});

	Ti.App.addEventListener('Set_Push', function(e) {
		win.remove(indicatorView);
		win.add(indicatorView);
		indicatorView.showInd();

		var id = Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']);
		var http = new ApiHttpClient();
		http.setPostShowOfflineDialogHook(function() {
			indicatorView.hideInd();
		});
		http.get(constants['API_PATH_FRIENDS_SETPUSH'], {
			'mezamasi_id' : id,
			'friend_id' : friendId,
			'is_push' : switchFlag
		});
		// 失敗時
		http.setOnerror(function() {
			indicatorView.hideInd();
			new ErrorPopup("通信に失敗しました。もう一度お試しください。");
			return;
		});
		http.setOnload(function() {
			indicatorView.hideInd();
			var setResult = JSON.parse(this.responseText).result;
			if (result == "RETRY" || result == "NG") {
				indicarorView.hideInd();
				new ErrorPopup("変更に失敗しました。もう一度お試しください。");
				return;
			}
		});
	});

	win.addEventListener('blur', function(e) {
		win.remove(indicatorView);
		win.remove(tableView);
	});

	return win;
}

module.exports = MemberListWindow;
