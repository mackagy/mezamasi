function ActivityWindow() {
	// 必要なモジュールを読み込み
	var style = require('style/activityStyle');
	var window = Ti.UI.createWindow(style['window']);
	
	return window;
}
module.exports = ActivityWindow;
