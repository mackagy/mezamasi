function AlarmWindow() {
	// 必要なモジュールを読み込み
	var style = require('style/alarmStyle');
	var constants = require('common/Constants');
	var DateFormat = require('common/DateFormat');
	var Cloud = require('modules/ti.cloud');
	var ApiHttpClient = require('common/ApiHttpClient');
	var IndicatorView = require('ui/common/indicatorView');
	var ErrorPopup = require('ui/common/errorPopup');
	var window = Ti.UI.createWindow(style['window']);
	
	//猶予(ミリ秒)
	var graceMicroSecond = 60000;
	//アラーム設定時
	var alarmMin = "00";
	//アラーム設定時
	var alarmHour = "00";
	// アラーム設定用基底ビュー
	var alarmPickerBaseView = Ti.UI.createView(style['alarmPickerBaseView']);
	// アラームピッカー用ビュー
	var alarmPickerView = Ti.UI.createView(style['alarmPickerView']);
	// アラームピッカー用底面ビュー
	var alarmPickerBottomView = Ti.UI.createView(style['alarmPickerBottomView']);
	// 起床ボタン用ビュー
	var wakeupButtonView = Ti.UI.createView(style['wakeupButtonView']);
	// インジケータ
	var ind = new IndicatorView();
	
	//時刻設定用ピッカー生成メソッド
	var createAlarmPicker = function() {
		var alarmPicker = Ti.UI.createPicker(style['alarmPicker']);
		//時
		var hours = Ti.UI.createPickerColumn();
		for (var i = 0; i <= 23; i++) {
			hours.addRow(Ti.UI.createPickerRow({title:"" + i}));	
		}
		//分
		var mins = Ti.UI.createPickerColumn();
		for (var j = 0; j <= 59; j++) {
			mins.addRow(Ti.UI.createPickerRow({title: j < 10 ? "0" + j : "" + j}));
		}
		alarmPicker.add([hours,mins]);
		alarmPicker.selectionIndicator = true;
		
		alarmPicker.addEventListener('change',function(e){
			if (e.columnIndex == 0) {
				alarmHour = e.row.title.length == 1 ? "0" + e.row.title : e.row.title;
			} else {
				alarmMin = e.row.title;
			}
		});
		return alarmPicker;
	}
	
	var getNextDay = function () {
	    var dt = new Date();
	    var baseSec = dt.getTime();
	    var addSec = 1 * 86400000;//日数 * 1日のミリ秒数
	    var targetSec = baseSec + addSec;
	    dt.setTime(targetSec);
	    return dt;
	}
	
	//アラーム設定ボタン作成メソッド
	var createSubmitButton = function() {
		var submitButton = Ti.UI.createButton(style["submitButton"]);
		submitButton.addEventListener('click',function(e){
			ind.showInd();
			
			// 現在の時分取得
			var current = new DateFormat('HHmm').format(new Date());
			var setting = alarmHour + "" + alarmMin;
		
			//設定時分が現在時分より小さい場合、年月日として明日を指定
			var settingYMD = null;
			if (current > setting) {
				var settingYMD = getNextDay();
			//設定時分が現在時分より大きい場合、年月日として本日を指定
			} else {
				var settingYMD = new Date();
			}
			
			settingYMD.setHours(alarmHour);
			settingYMD.setMinutes(alarmMin);
			settingYMD.setSeconds(0);
			
			// 救援push通知タイトル
			var title = 'オコシテ'
			// 救援push通知メッセージ;
			var dateFMT = new DateFormat("yyyy/MM/dd HH:mm");
			var message = dateFMT.format(settingYMD)
						+ 'に起床予定の' 
						+ Ti.App.Properties.getString(constants["FB_NAME_KEY"])
						+ 'さんがオコシテ欲しいようです。';
			// 起床設定
			var client = new ApiHttpClient();
			client.setPostShowOfflineDialogHook(function() {
				ind.hideInd();	
			});
			client.get(constants['API_PATH_NOTICE_RESERVATION'], {
            	'mezamasi_id' : Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']),
            	'notice_time' : dateFMT.format(settingYMD),
            	'title' : title,
            	'message': message
            });
            // 成功時
            client.setOnload(function(){
            	var json = JSON.parse(this.responseText);       		
	        	if (json.result == 'NG') {
	        		new ErrorPopup("オコシテできませんでした。もう一度お試しください。");
	        		ind.hideInd();
	        		return;
				}
				//---------- もろもろ起床設定ここから ----------//
            	Ti.App.Properties.setString(constants['ALERT_TIME'],dateFMT.format(settingYMD));
            	Ti.App.Properties.setString(constants['IS_NEED_NOTIFY'],'1');
            	//---------- もろもろ起床設定ここまで ----------//
            	ind.hideInd();
				var dialog = Ti.UI.createAlertDialog({
				message: 'オコシテしました。',
				buttonNames: ['OK']});
				dialog.show();
				Ti.App.fireEvent("TopWindow_switchView");
				switchView();
            });
            // 失敗時
            client.setOnerror(function(){
            	new ErrorPopup("オコシテできませんでした。もう一度お試しください。");
            	ind.hideInd();
            	return;
            });
		});	
		return submitButton;
	}
	
	// 起床ボタン作成
	var createWakeupButton = function() {
		var wakeupButton = Ti.UI.createButton(style["wakeupButton"]);
		wakeupButton.addEventListener('click',function(e){
			wakeupProcess();
		});	
		return wakeupButton;
	}
	
	// 画面構成要素の初期化
	var initView = function() {
		var alarmPicker = createAlarmPicker();
		var submitButton = createSubmitButton();
		
		alarmPickerView.add(alarmPicker);
		alarmPickerBottomView.add(submitButton);
		alarmPickerBaseView.add(alarmPickerView); 
		alarmPickerBaseView.add(alarmPickerBottomView);
		window.add(alarmPickerBaseView);

		var wakeupButton = createWakeupButton();
		wakeupButtonView.add(wakeupButton);
		window.add(wakeupButtonView);
	}

	// 状態に応じた画面の切替
	var switchView = function() {
		if (!Ti.App.Properties.hasProperty(constants['ALERT_TIME'])) {
			alarmPickerBaseView.show();
			wakeupButtonView.hide();
		} else {
			alarmPickerBaseView.hide();
			wakeupButtonView.show();			
		}
	}
	
	// 起床時の処理
	var wakeupProcess = function() {
		// インジケータ表示
		ind.showInd();
		
		var current = new Date();
		var alarm = new Date(Ti.App.Properties.getString(constants["ALERT_TIME"]));
		var dialog;
		if (current.getTime() - graceMicroSecond > alarm.getTime()) {
			// 起床失敗ダイアログ
			dialog = Ti.UI.createAlertDialog(style["wakeupFailureDialog"]);		
		} else {
			// 起床成功ダイアログ
			dialog = Ti.UI.createAlertDialog(style["wakeupSuccessDialog"]);		
		}
		// 起床取り消し
		var client = new ApiHttpClient();
		client.setPostShowOfflineDialogHook(function() {
			ind.hideInd();	
		});
		client.get(constants['API_PATH_NOTICE_DEFEASIBLE'], {
           	'mezamasi_id' : Ti.App.Properties.getString(constants['MEZAMASI_ID_KEY']),
         });
		// 通信成功
		client.setOnload(function(){
			var json = JSON.parse(this.responseText);       		
			if (json.result == 'NG') {
				new ErrorPopup("オキれませんでした。もう一度お試しください。");
				ind.hideInd();
				return;
			}
			dialog.show();
            
			//---- もろもろ削除ここから -----//
			Ti.App.iOS.cancelAllLocalNotifications();
			Ti.App.Properties.removeProperty(constants['ALERT_TIME']);
			Ti.App.Properties.removeProperty(constants['IS_NEED_NOTIFY']);
			//----- もろもろ削除ここまで ------//
			
			ind.hideInd();
			Ti.App.fireEvent("TopWindow_switchView");
			switchView();
		});
		// 通信失敗
		client.setOnerror(function(){
			new ErrorPopup("オキれませんでした。もう一度お試しください。");
			ind.hideInd();
			return;
		});
	}
	
	Ti.App.iOS.addEventListener('notification', function(e) {
		wakeupProcess();
	});
	
	//------ メイン処理 ------//
	initView();
	switchView();

	window.add(ind);
	return window;
}
module.exports = AlarmWindow;
