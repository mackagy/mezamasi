function TopWindow() {
	// 必要なモジュールを読み込み
	var DateFormat = require('common/DateFormat');
	var ApiHttpClient = require('common/ApiHttpClient');
	var Cloud = require('modules/ti.cloud');
	var constants = require('common/Constants');
	var style = require('style/topStyle');
	var window = Ti.UI.createWindow(style['window']);
	
	// 通常時ビュー
	var defaultView = Ti.UI.createView(style['defaultView']);
	// 目覚まし設定後ビュー
	var alarmView = Ti.UI.createView(style['alarmView']);
	// 目覚ましラベル
	var alarmLabel = Ti.UI.createLabel(style['alarmLabel']);
	// 目覚まし画像作成
	var jikanImage = Titanium.UI.createImageView(style['jikanImage']);
	// 目覚まし務設定画像作成
	var nojikanImage = Titanium.UI.createImageView(style['nojikanImage']);
	
	// 画面初期化
	var initView = function() {
		defaultView.add(nojikanImage);
		window.add(defaultView);
		alarmView.add(jikanImage);
		alarmView.add(alarmLabel);
		window.add(alarmView);
	}
	
	// 画面切り替え
	var switchView = function() {
		// 目覚ましが設定されている場合
		if(Ti.App.Properties.hasProperty(constants["ALERT_TIME"])) {
			defaultView.hide();
			alarmLabel.text = Ti.App.Properties.getString(constants["ALERT_TIME"]);
			alarmView.show();
		} else {
			defaultView.show();
			alarmView.hide();
		}
	}
	
	// リスナ
	Ti.App.addEventListener('TopWindow_switchView', function(e)	{
		switchView();		
	});
	
	//----- メイン処理 -----//	
	initView();
	switchView();
    
	return window;
}
module.exports = TopWindow;
