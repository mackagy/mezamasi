function ErrorPopup(message) {
	
	if (message == null) {
		message = '通信に失敗しました。もう一度お試しください。';
	}
	
	var dialog = Titanium.UI.createAlertDialog({
		title : "エラー",
		message: message,
		buttonNames : ['OK']
	});

	dialog.show();
}
module.exports = ErrorPopup;

        
        